#include <DHT11.h>
/*
* Definir variables de pin
*/

int pin = 2;
int pinLum = A0;
int pinHSoil = A1;
int led_hum = 4;
int led_luz = 3;
/*
*Definir variables del sistema
*/
char nom_sensor[4] = {'T', 'H', 'L', 'S' };
float mediciones[4]={0, 0, 0, 0};
float lum;
float hum_soil;
float temp;
float hum;
float hum_min = 50;
int error;
bool on_luz = true;


DHT11 dht11(pin); 


void setup()
{
  
  Serial.begin(9600);
  pinMode(led_luz, OUTPUT);
  pinMode(led_hum, OUTPUT);
  while (!Serial) { }
}

void loop()
{
  
  lum = map(analogRead(pinLum), 0, 520, 0, 99);
  hum_soil = map(analogRead(pinHSoil), 200, 690, 99, 0);
  error = dht11.read(hum, temp);
  //if (Serial.available() != 0) {
    if(error == 0){
        imprimir(temp, hum, lum, hum_soil);
      }else{
        Serial.println(error);  
      }
    //}
      //regar(hum_soil, hum_min);
    //encender_luz(on_luz);
   delay(5000);
  
}

    void regar(float hum_soil, float hum_min){
        
        if(hum_soil < hum_min){
            //Serial.println(hum_soil);
            digitalWrite(led_hum, HIGH);
            delay(100);
            digitalWrite(led_hum, LOW);
        }

    }


    void encender_luz(bool on_luz){
        if(on_luz){
            digitalWrite(led_luz, HIGH);
        }
        else{
            digitalWrite(led_luz, LOW);
        }
    }


    void imprimir(float temp, float hum, float lum, float hum_soil){
           Serial.print("TEMP:");
           Serial.print(temp);
           Serial.print(",HUM:");
           Serial.print(hum);
           Serial.print(",LUZ:");
           Serial.print(lum);
           Serial.print(",HUM_SOIL:");
           Serial.print(hum_soil);
           Serial.println();
           //Serial.println();
       //}
       //else{
           //Serial.print("Error al medir No:");
           //Serial.print(error);
           //Serial.println();   
       }
  //}
 
