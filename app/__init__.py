#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask, render_template, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from config import Config

#Define the WSGI application object
app = Flask(__name__)


#configurations
app.config.from_object(Config)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql:///huertechno'


#import database
db = SQLAlchemy(app)


#Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


#Blueprints
from app.home.controllers import home_blue
app.register_blueprint(home_blue)

#database
db.create_all()

