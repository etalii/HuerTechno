#-*- coding: utf-8 -*-
from app.home.models import Mediciones
from app import db
import json
from serial import Serial
from threading import Thread
import time




class MeasureGetter(Thread):


    mediciones = {
            'TEMP' : 0,
            'HUM' : 0,
            'LUZ' : 0,
            'HUM_SOIL' : 0
            }


    def __init__(self, fname):
        super().__init__()
        self.fname = fname

    def run(self):
        print("run!")
        while True:
            self.arduino = Serial(self.fname)
            med_sensor = self.arduino.readline().decode("utf-8")[:-2]
            med_sensor = med_sensor.split(',')
            if len(med_sensor) == 4:
                for sensor in med_sensor:
                    try:
                        nom, value = sensor.split(':')
                        self.mediciones[nom] = value
                    except:
                        pass
        print(self.mediciones)



#class SensorsThread(threading.Thread):
#
#
#
#    informacion = {}
#    name_sensor=['TEMP', 'HUM', 'LUZ', 'HUM_SOIL']
#    mediciones = {
#            'TEMP' : 0,
#            'HUM' : 0,
#            'LUZ' : 0,
#            'HUM_SOIL' : 0
#            }
#    #arduino = Serial()
#    TASA_TIME = 5
#    fname = ""
#    read_loop = True
#    arduino = Serial()
#
#    def __init__(self, Serial):
#        #self.fname = fname
#        self.arduino = Serial
#
#    def read_sensor(self):
#        while True:
#            #arduino = self.Serial(self.fname)
#            med_sensor = self.arduino.readline().decode("utf-8")[:-2]
#            #print(med_sensor)
#            med_sensor = med_sensor.split(',')
#            for sensor in med_sensor:
#                nom, value = sensor.split(':')
#                self.mediciones[nom] = value
#            self.arduino.close()
#            time.sleep(1)
#        return self.mediciones
#
#    def insert_data(self):
#        while True:
#            self.read_sensor()
#        #while self.read_loop is True:
#        #    self.informacion['time'] = time.strftime("%d/%m/%Y %H:%M:%S")
#        #    self.informacion['mediciones'] = self.read_sensor()
#        #    med_sensor = Mediciones(med_sensor=json.dumps(self.informacion))
#        #    db.session.add(med_sensor)
#        #    db.session.commit()
#        #time.sleep(1)
#        return True
#
#
#    def start(self):
#        self.insert_data()
#        self.read_loop = True
#        #t = Thread(target=self.insert_data)
#        #t.start()
#
#    def stop(self):
#        self.read_loop = False
#


