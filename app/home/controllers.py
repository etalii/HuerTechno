from flask import Blueprint, render_template
from app.home.models import Mediciones
from app import db
import json



#from app.home.sensors import Sensors



#define blueprint
home_blue = Blueprint('home', __name__)



#set the route
@home_blue.route('/')
def index():
    return render_template('home/index3.html')


#mediciones
#S = Sensors('/dev/ttyUSB0')
#S.start()


