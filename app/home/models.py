#-*- coding: utf-8 -*-
from app import db
#from huerta import Huerta
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy import  Column, Integer, JSON, DateTime

class Base(db.Model):

    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())


class Mediciones(Base):


    __tablename__= 'measure'

    med_sensor = db.Column(JSON)
    #med objeto JSON






